call plug#begin ()

Plug 'mbbill/undotree'

nnoremap U :UndotreeToggle<CR>

let g:airline_powerline_fonts = 1
set laststatus=2

Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'

Plug 'valloric/youcompleteme'

Plug 'ctrlpvim/ctrlp.vim'
Plug 'scrooloose/nerdtree'

nnoremap g :NERDTreeToggle<CR>

set nu
highlight LineNr term=bold cterm=NONE ctermfg=DarkGrey ctermbg=NONE gui=NONE guifg=DarkGrey guibg=NONE

Plug 'thinca/vim-localrc'

call plug#end ()

